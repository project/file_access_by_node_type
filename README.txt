
Summary
=======
File access by node type allows website administrators to select which roles can
access files belonging to nodes of a certain node type. This goes beyond simple
field permission, since there can be situations where users might embed an
uploaded file into a Body textarea, or a WYSIWYG editor.

This module allows the restriction to work even in those cases by intercepting
the file download before it happens. It also respects other access control
mechanisms, since it defaults to either restricting access, or delegating it
further. If the file download is not denied, and no other module is there to
make a second decision, then it will download it. This allows for future
integration with other modules and should be consistent in how Drupal handles
access control.

To use
======
Enable this module as usual, then go to
http://www.example.com/admin/settings/file_access_by_node_type to configure the
different roles and node types.

Credits
=======
This module was developed for Switchback by:
* Stephen Colson (stephen.colson) - http://www.switchbackcms.com
* Victor Kareh (vkareh) - http://www.vkareh.net

